#ifndef __PILHA_H__
#define __PILHA_H__

#include <stdio.h>
#include <stdlib.h>

struct pilha{
 int num;
 struct pilha *prox;
};
typedef struct pilha Pilha;

int tam_pilha;

void iniciar_pilha(Pilha* PILHA);
Pilha *alocar_pilha(int valor);
int vazia_pilha(Pilha* PILHA);
void exibir_pilha(Pilha* PILHA);
void liberar_pilha(Pilha* PILHA);
void push_pilha(Pilha* PILHA, int valor);
Pilha *pop_pilha(Pilha* PILHA);

#endif //__PILHA_H__