#include "Grafo.h"

main(){
	grafo* g;
	g = criar_grafo(1,0,6);
	inserir_aresta(g, 0,1,4);
	inserir_aresta(g, 0,2,1);
	inserir_aresta(g, 1,3,4);
	inserir_aresta(g, 1,2,4);
	inserir_aresta(g, 2,3,2);
	inserir_aresta(g, 3,0,3);
	inserir_aresta(g, 3,4,4);
	inserir_aresta(g, 3,5,6);
	inserir_aresta(g, 4,5,5);

	imprimir_arestas(g);

	kruskal(g);
	prim(g, 0);
	dijkstra(g, 0);
	floyd_warshall(g);
}