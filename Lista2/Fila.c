#include "Fila.h"

int tam_fila;

void iniciar_fila(Fila *FILA){
   FILA->prox = NULL;
   tam_fila=0;
}

int vazia_fila(Fila *FILA){
   if(FILA->prox == NULL){
      return 1;
   } else {
      return 0;
   }
}

Fila* alocar_fila(int valor){
   Fila* novo = (Fila*) malloc(sizeof(Fila));
   if(!novo){
      printf("Sem memoria disponivel!\n");
      exit(1);
   } else {
      novo->num = valor;
      return novo;
   }
}

void inserir_fila(Fila *FILA, int valor){
   Fila* novo = alocar_fila(valor);
   novo->prox = NULL;

   if(vazia_fila(FILA) == 1){
      FILA->prox=novo;
   } else {
      Fila* tmp = FILA->prox;
      while(tmp->prox != NULL){
         tmp = tmp->prox;
      }
      tmp->prox = novo;
   }
   tam_fila++;
}


Fila* retirar_fila(Fila* FILA){
   if(FILA->prox == NULL){
      printf("Fila ja esta vazia\n");
      return NULL;
   } else {
      Fila *tmp = FILA->prox;
      FILA->prox = tmp->prox;
      tam_fila--;
      return tmp;
   }

}

void exibir_fila(Fila* FILA){
   if(vazia_fila(FILA)){
      printf("Fila vazia!\n\n");
      return;
   }
   Fila* tmp;
   tmp = FILA->prox;
   printf("Fila :");
   while( tmp != NULL){
      printf("%5d", tmp->num);
      tmp = tmp->prox;
   }
   printf("\n        ");
   int count;
   for(count=0 ; count < tam_fila ; count++){
      printf("  ^  ");
   }
   printf("\nOrdem:");
   for(count=0 ; count < tam_fila ; count++){
      printf("%5d", count+1);
   }

   printf("\n\n");
}

void liberar_fila(Fila* FILA){
   if(!vazia_fila(FILA)){
      Fila *proxFila,
           *atual;
      atual = FILA->prox;
      while(atual != NULL){
         proxFila = atual->prox;
         free(atual);
         atual = proxFila;
      }
   }
}