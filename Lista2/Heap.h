#ifndef __HEAP_H__
#define __HEAP_H__

#include <stdio.h>
#include <stdlib.h>
#include "Grafo.h"

#define comprimento 999999

struct  heap {
	int tam;
	tabela* vetor[comprimento];
}; typedef struct heap heap;

int pai(int i);
int esq(int i);
int dir(int i);
void build_min_heap(heap* h);
void trocar(heap* h, int x, int y);
void min_heapify(heap* h, int i);
tabela* heap_minimum(heap* h);
void heap_increase_key(heap* h, int i, int chave);
void  min_heap_insert(heap* h, int chave);
void insert(heap* h, tabela*);
void mostrar(heap* h);
tabela* heap_extract_min(heap* h);
int heap_empty(heap* h);

#endif //__HEAP_H__
