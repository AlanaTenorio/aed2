#ifndef __GRAFO_H__
#define __GRAFO_H__

#include <stdio.h>
#include <stdlib.h>
#include "Lista.h"

#define MAXN 50
#define MAXM 100
#define true 1
#define false 0

typedef int flag;

struct  grafo {
	int m; //Número de arestas
	int V[MAXN][MAXN]; //Matriz de adjacência
	int nVertices; //Representa a quantidade de vértices que o gráfico possui
	flag eh_ponderado;
	flag eh_digrafo;
	lista* listaArestas;

}; typedef struct grafo grafo;

struct tupla { //Tupla para armazenar o vetor de vizinhos
	int vetor[MAXN];
	int n;
}; typedef struct tupla tupla;

struct tabela { //Tabela utilizada nos algoritmos de Prim e Dijkstra para armazenar o vertice, custo e seu anterior
	int vertice;
	int custo;
	int anterior;
}; typedef struct tabela tabela;

grafo* criar_grafo(flag eh_ponderado, flag eh_digrafo, int nVertices);
void inserir_aresta(grafo* g, int orig, int dest, int peso);
void imprimir_arestas(grafo* g);
int busca_largura(grafo* g, int v, int p);
int busca_profundidade(grafo* g, int v, int j);
void kruskal(grafo* g);
void prim(grafo* g, int v0);
void dijkstra(grafo* g, int v0);
void floyd_warshall(grafo* g);

#endif //__GRAFO_H__
