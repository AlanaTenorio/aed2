#include "Lista.h"

void inicia_lista(lista *LISTA){
	LISTA->prox = NULL;
	tam=0;
}

int vazia_lista(lista *LISTA){
	if(LISTA->prox == NULL){
		return 1;
	}
	else{
		return 0;
	}
}

void insere_ordenado_lista(lista* LISTA, int vertice1, int vertice2, int peso){
	lista* novo = (lista*) malloc(sizeof(lista));
	novo->vertice1 = vertice1;
	novo->vertice2 = vertice2;
	novo->peso = peso;
	novo->prox = NULL;
	
	lista* atual;
	atual = LISTA;
	
	if(atual == NULL || atual->peso >= novo->peso){
		novo->prox = atual;
	} else{
		while(atual->prox != NULL && atual->prox->peso < novo->peso){
			atual = atual->prox;
		}
		novo->prox = atual->prox;
		atual->prox = novo;
	}
}

void exibe_lista(lista* LISTA){
	system("clear");
	if(vazia_lista(LISTA)){
		printf("Lista vazia!\n\n");
		return ;
	}
	
	lista* tmp;
	tmp = LISTA->prox;
	printf("Lista:\n");
	while( tmp != NULL){
		printf("Vertice1: %5d\n", tmp->vertice1);
		printf("Vertice2: %5d\n", tmp->vertice2);
		printf("Peso: %5d\n", tmp->peso);
		tmp = tmp->prox;
	}
	printf("\n        ");
	int count;
	for(count=0 ; count < tam ; count++){
		printf("  ^  ");
	}
	printf("\nOrdem:");
	for(count=0 ; count < tam ; count++){
		printf("%5d", count+1);
	}
	
		
	printf("\n\n");
}

void libera_lista(lista* LISTA){
	if(!vazia_lista(LISTA)){
		lista *proxlista,
			  *atual;
		
		atual = LISTA->prox;
		while(atual != NULL){
			proxlista = atual->prox;
			free(atual);
			atual = proxlista;
		}
	}
}

lista* retiraInicio_lista(lista* LISTA){
	if(LISTA->prox == NULL){
		printf("Lista ja esta vazia\n");
		return NULL;
	}else{
		lista* tmp = LISTA->prox;
		LISTA->prox = tmp->prox;
		tam--;
		return tmp;
	}
				
}

lista* retiraFim(lista* LISTA){
	if(LISTA->prox == NULL){
		printf("Lista ja vazia\n\n");
		return NULL;
	}else{
		lista *ultimo = LISTA->prox,
			 *penultimo = LISTA;
			 
		while(ultimo->prox != NULL){
			penultimo = ultimo;
			ultimo = ultimo->prox;
		}
			 
		penultimo->prox = NULL;
		tam--;
		return ultimo;		
	}
}
