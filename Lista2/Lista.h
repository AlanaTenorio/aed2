#ifndef __LISTA_H__
#define __LISTA_H__

#include <stdio.h>
#include <stdlib.h>

struct Lista{
	int vertice1;
	int vertice2;
	int peso;
	struct Lista *prox;
}; 
typedef struct Lista lista;

int tam;

void inicia_lista(lista *LISTA);
void insere_ordenado_lista(lista *LISTA, int vertice1, int vertice2, int peso);
void exibe_lista(lista *LISTA);
void libera_lista(lista *LISTA);
lista *retiraInicio_lista(lista *LISTA);
lista *retiraFim_lista(lista *LISTA);

#endif //__LISTA_H__