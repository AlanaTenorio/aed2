#include "Heap.h"

int pai(int i){
	return ((i+1)/2)-1;
}

int esq(int i){
	return (2*(i+1)-1);
}

int dir(int i){
	return 2*(i+1);
}

void build_min_heap(heap* h){
	int i;
	for (i = ((h->tam-1)/2); i >= 0; i--){
		min_heapify(h, i);
	}
}

void trocar(heap* h, int x, int y){
	tabela* aux; 
	aux = h->vetor[x];
	h->vetor[x] = h->vetor[y];
	h->vetor[y] = aux;
}

void min_heapify(heap* h, int i){
	int l = esq(i);
	int r = dir(i);
	int menor;
	if (l < h->tam && h->vetor[l]->custo < h->vetor[i]->custo){
		menor = l;
	}
	else{
		menor = i;
	}
	if (r < h->tam && h->vetor[r]->custo < h->vetor[menor]->custo){
		menor = r;
	}
	if (menor != i){
		trocar(h, i, menor);
		min_heapify(h, menor);
	}
}

tabela* heap_minimum(heap* h){
	return h->vetor[0];
}

tabela* heap_extract_min(heap* h){
	tabela* min;
	if(h->tam <= 0){
		fprintf(stderr, "\nErro");
		return NULL;
	}
	h->tam = h->tam-1;
	min = h->vetor[0];
	h->vetor[0] = h->vetor[h->tam];
	min_heapify(h, 0);
	return min;
}

void heap_increase_key(heap* h, int i, int chave){
	if (chave < h->vetor[i]->custo){
		fprintf(stderr, "\nErro");
	}else{
		h->vetor[i]->custo = chave;
		while (i > 0 && h->vetor[pai(i)]->custo > h->vetor[i]->custo){
			trocar(h, i, pai(i));
			i = pai(i);
		}
	}
}

void min_heap_insert(heap* h, int chave){
	h->tam = h->tam + 1;
	h->vetor[h->tam]->custo = -9999999;
	heap_increase_key(h, h->tam-1, chave);
}

void insert(heap* h, tabela* t){ //Insere de forma não ordenada
	h->vetor[h->tam] = t;
	h->tam = h->tam + 1;
}

int heap_empty(heap* h){
	if(h->tam != 0){
		return 0;
	}
	return 1;
}

void mostrar(heap* h){
	int i;
	for (i = 0; i < h->tam; i++){
		printf("Vertice: %d\n", h->vetor[i]->vertice);
		printf("Custo: %d\n", h->vetor[i]->custo);
		printf("Vertice anterior: %d\n", h->vetor[i]->anterior);
	}
}

