#include "Grafo.h"
#include "Fila.c"
#include "Pilha.c"
#include "Heap.c"
#include "Lista.c"

grafo* criar_grafo(flag eh_ponderado, flag eh_digrafo, int nVertices){
	grafo* g =(grafo*) malloc(sizeof(grafo));
	g->m = 0;
	g->nVertices = nVertices;
	g->listaArestas = (lista*) malloc(sizeof(lista));
	inicia_lista(g->listaArestas);
	int i, j;
	for(i = 0; i < nVertices; i++){
		for(j = 0; j < nVertices; j++){
			g->V[i][j] = 0;
		}
	}
	g->eh_ponderado = eh_ponderado;
	g->eh_digrafo = eh_digrafo;
	return g;
}

void inserir_aresta(grafo* g, int orig, int dest, int peso){
	g->m++;
	insere_ordenado_lista(g->listaArestas, orig, dest, peso);
	if(g->eh_ponderado == true && g->eh_digrafo == true){ //Se o grafo é ponderado, a matriz recebe o valor do peso
		g->V[orig][dest] = peso;
	} else if (g->eh_ponderado == true && g->eh_digrafo == false){
		g->V[orig][dest] = peso;
		g->V[dest][orig] = peso;
	} else if (g->eh_ponderado == false && g->eh_digrafo == true){
		g->V[orig][dest] = 1;
	} else if (g->eh_ponderado == false && g->eh_digrafo == false){
		g->V[orig][dest] = 1;
		g->V[dest][orig] = 1;
	}
}

void imprimir_arestas(grafo* g){
	int i, j;
	for(i = 0; i < g->nVertices; i++){
		for(j = 0; j < g->nVertices; j++){
			if(g->V[i][j] != 0 && g->eh_ponderado == false){
				fprintf(stderr, "Vertices Ligados: %d e %d \n",i,j);
			} else if (g->V[i][j] != 0 && g->eh_ponderado == true){
				fprintf(stderr, "Vertices Ligados: %d e %d, peso da aresta = %d \n",i,j,g->V[i][j]);
			}
		}
	}
}

//Retorna uma tupla, contendo o vetor de todos os vizinhos de v e seu tamanho
tupla* buscar_vizinhos(grafo* g, int v){ 
	tupla* t = (tupla*) malloc(sizeof(tupla));
	int i, j;
	int n = 0;
	int w;
	for(w = 0; w < g->nVertices; w++){
		t->vetor[w] = -1;
	}

	//Imprimindo os vizinhos
	/*for(i = 0; i < g->nVertices; i++){
		if(g->V[i][v] != 0){
			fprintf(stderr, "V[%d][%d], peso: %d\n", i, v, g->V[i][v]);
			t->vetor[n] = i;
			n++;
		}
	}
	for(i = 0; i < g->nVertices; i++){
		if(g->V[v][i] != 0){
			fprintf(stderr, "V[%d][%d], peso: %d\n", i, v, g->V[i][v]);
			t->vetor[n] = i;
			n++;
		}
	}*/
	t->n = n;
	return t;
}

//Procura no grafo um vértice, utilizando um vértice inicial
int busca_largura(grafo* g, int v, int p){
	int i, w;
	Fila* u = NULL;
	int m [g->nVertices];
	int flag = false;
	Fila* f = (Fila*)malloc(sizeof(Fila));
	iniciar_fila(f);
	for(i = 0; i < g->nVertices; i++){
		m[i] = 0;
	}
	inserir_fila(f, v);
	m[v] = 1;
	while(vazia_fila(f) != true){
		u = retirar_fila(f);
		if(p == u->num){
			flag = true;
		}
		m[u->num] = 1;
		tupla* t = buscar_vizinhos(g, u->num);
		for(w = 0; w < t->n; w++){
			if(m[t->vetor[w]] == 0){
				m[t->vetor[w]] = 1;
				inserir_fila(f, t->vetor[w]);
			}
		}
	}
	return flag;
}

int busca_profundidade(grafo* g, int v, int j){
	int m [g->nVertices];
	int i, w;
	Pilha* u = NULL;
	int flag = false;
	Pilha* p = (Pilha*)malloc(sizeof(Pilha));
	iniciar_pilha(p);
	for (i = 0; i < g->nVertices; i++){
		m[i] = 0;
	}
	push_pilha(p, v);
	m[v] = 1;
	while(vazia_pilha(p) != true){
		u = pop_pilha(p);

		tupla* t = buscar_vizinhos(g, u->num);
		if(j == u->num){
			flag = true;
		}
		m[u->num] = 1;
		for(w = 0; w < t->n; w++){
			if(m[t->vetor[w]] == 0){
				m[t->vetor[w]] = 1;
				push_pilha(p, t->vetor[w]);
			}
		}
	}
	return flag;
}

//Algoritmo de Kruskal para árvore geradora mínima
void kruskal(grafo* g){
	int comp[MAXM];
	int i, u, v, j;
	lista* aux;
	int peso = 0;

	for (i = 0; i < MAXM; i++){
		comp[i] = i;
	}

	printf("Árvore geradora mínima (KRUSKAL): \n");
	for (aux = g->listaArestas; aux != NULL; aux = aux->prox){
		if(comp[aux->vertice1] != comp[aux->vertice2]){
			u = comp[aux->vertice1];
			v = comp[aux->vertice2];
			for(j = 0; j < MAXM; j++){
				if(comp[j] == v){
					comp[j] = u;
				}
			}
			printf("Vértice: %d e vértice: %d, com peso: %d\n", aux->vertice1, aux->vertice2, aux->peso);
			peso += aux->peso;
		}
	}
	printf("Peso total: %d\n", peso);
}

//Algortimo de Prim para árvore geradora mínina
void prim(grafo* g, int v0){
	int i;
	int z;
	int peso = 0;
	heap* hp = (heap*)malloc(sizeof(heap));
	tabela* t[g->nVertices];
	tabela* u;
	lista* aux;
	for(i = 0; i < g->nVertices; i++){
		t[i] = (tabela*)malloc(sizeof(tabela));
		t[i]->vertice = i;
		t[i]->custo = 999999;
		t[i]->anterior = -1;
		if(i == v0){
			t[v0]->custo = 0;
		}
		insert(hp, t[i]); //Insere na heap
	}

	build_min_heap(hp); //Constroi a fila de prioridade
	printf("Árvore geradora mínima (PRIM): \n");
	while(heap_empty(hp) != true){
		u = heap_extract_min(hp);
		for(aux = g->listaArestas->prox; aux != NULL; aux = aux->prox){
			if (aux->vertice1 == u->vertice || aux->vertice2 == u->vertice){
				if (aux->vertice1 == u->vertice){
					z = aux->vertice2;
				} else if(aux->vertice2 == u->vertice){
					z = aux->vertice1;
				}				
				if(t[z]->custo > aux->peso){
					t[z]->custo = aux->peso;
					t[z]->anterior = u->vertice;
					build_min_heap(hp); //Atualiza a fila de prioridade
				}
			}
		}
		if(u->anterior != -1){
			printf("Vértice: %d e vértice: %d, com peso: %d\n", u->vertice, u->anterior, u->custo);
			peso += u->custo;
		}
	}
	printf("Peso total: %d\n", peso);
}

//Algoritmo de Dijkstra para o menor caminho entre os vértices
void dijkstra(grafo* g, int v0){
	int i;
	int z;
	int peso = 0;
	heap* hp = (heap*)malloc(sizeof(heap));
	tabela* t[g->nVertices];
	tabela* u;
	lista* aux;
	for(i = 0; i < g->nVertices; i++){
		t[i] = (tabela*)malloc(sizeof(tabela));
		t[i]->vertice = i;
		t[i]->custo = 999999;
		t[i]->anterior = -1;
		if(i == v0){
			t[v0]->custo = 0;
		}
		insert(hp, t[i]);
	}
	build_min_heap(hp);
	printf("(DIJKSTRA): \n");
	while(heap_empty(hp) != true){
		u = heap_extract_min(hp);
		for(aux = g->listaArestas->prox; aux != NULL; aux = aux->prox){
			if (aux->vertice1 == u->vertice || aux->vertice2 == u->vertice){
				if (aux->vertice1 == u->vertice){
					z = aux->vertice2;
				} else if(aux->vertice2 == u->vertice){
					z = aux->vertice1;
				}	
				if(t[z]->custo > u->custo + aux->peso){
					t[z]->custo = u->custo + aux->peso;
					t[z]->anterior = u->vertice;
					build_min_heap(hp);
				}
			}
		}
	}
	int j;
	for(j = 0; j < g->nVertices; j++){
		if(t[j]->anterior != -1){
			printf("Menor caminho entre %d e %d, com peso: %d\n", v0, t[j]->vertice, t[j]->custo);
		}
	}
}

//Algoritmo de floyd warshall que exibe uma matriz VxV com a menor distância entre um vértice e outro
void floyd_warshall(grafo* g){
	int i, j, w;
	int matriz[g->nVertices][g->nVertices];
	for(i = 0; i < g->nVertices; i++){
		for(j = 0; j < g->nVertices; j++){
			matriz[i][j] = g->V[i][j];
			if(matriz[i][j] == 0){
				matriz[i][j] = 99999;
			}
		}
	}
	printf("Matriz representando as distâncias mínimas entre os vértices (FLOYD WARSHALL): \n");
	for(w = 0; w < g->nVertices; w++){
		for(i = 0; i < g->nVertices; i++){
			for(j = 0; j < g->nVertices; j++){
				if(i == j){
					matriz[i][j] = 0;
				} else 
 				if (matriz[i][w] + matriz[w][j] < matriz[i][j]){
          			matriz[i][j] = matriz[i][w] + matriz[w][j];
 				}
			}
		}
	}
	
	for(i = 0; i < g->nVertices; i++){
		for(j = 0; j < g->nVertices; j++){
			if(matriz[i][j] != 99999){
				printf(" %d ", matriz[i][j]);
			}
		}
		printf("\n");
	}
	
}