#ifndef __FILA_H__
#define __FILA_H__

#include <stdio.h>
#include <stdlib.h>

struct fila{
   int num;
   struct fila *prox;
};
typedef struct fila Fila;

void iniciar_fila(Fila* FILA);
int vazia_fila(Fila* FILA);
Fila* alocar_fila(int valor);
void inserir_fila(Fila* FILA, int valor);
Fila* retirar_fila(Fila* FILA);
void exibir_fila(Fila* FILA);
void liberar_fila(Fila* FILA);

#endif //__FILA_H__