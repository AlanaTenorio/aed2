#include "Pilha.h"


void iniciar_pilha(Pilha* PILHA){
	PILHA->prox = NULL;
	tam_pilha=0;
}

int vazia_pilha(Pilha* PILHA){
	if(PILHA->prox == NULL){
		return 1;
	} else {
		return 0;
	}
}

Pilha* alocar_pilha(int valor){
	Pilha* novo=(Pilha*) malloc(sizeof(Pilha));
	if(!novo){
		printf("Sem memoria disponivel!\n");
		exit(1);
	} else {
		novo->num = valor;
		return novo;
	}
}

void exibir_pilha(Pilha* PILHA){
	if(vazia_pilha(PILHA)){
		printf("PILHA vazia!\n\n");
	} else {
		Pilha* tmp;
		tmp = PILHA->prox;
		printf("PILHA:");
		while( tmp != NULL){
			printf("%5d", tmp->num);
			tmp = tmp->prox;
		}
		printf("\n        ");
		int count;
		for(count=0 ; count < tam_pilha ; count++){
			printf("  ^  ");
		}
		printf("\nOrdem:");
		for(count=0 ; count < tam_pilha ; count++){
			printf("%5d", count+1);
		}
		printf("\n\n");
	}

}

void liberar_pilha(Pilha* PILHA){
	if(!vazia_pilha(PILHA)){
		Pilha *proxPilha,
		*atual;
		atual = PILHA->prox;
		while(atual != NULL){
			proxPilha = atual->prox;
			free(atual);
			atual = proxPilha;
		}
	}
}

void push_pilha(Pilha* PILHA, int valor){
	Pilha* novo = alocar_pilha(valor);
	novo->prox = NULL;

	if(vazia_pilha(PILHA)){
		PILHA->prox=novo;
	} else {
		Pilha *tmp = PILHA->prox;
		while(tmp->prox != NULL){
			tmp = tmp->prox;
		}
		tmp->prox = novo; 
	}
	tam_pilha++;
}


Pilha* pop_pilha(Pilha* PILHA){
	if(PILHA->prox == NULL){
		printf("PILHA ja vazia\n\n");
		return NULL;
	} else {
		Pilha* ultimo = PILHA->prox;
		Pilha* penultimo = PILHA;
		
		while(ultimo->prox != NULL){
			penultimo = ultimo;
			ultimo = ultimo->prox;
		}
			
		penultimo->prox = NULL;

		tam_pilha--;
		return ultimo;
	}
}