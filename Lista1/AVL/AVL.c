#include "AVL.h"
#include <stdio.h>
#include <stdlib.h>

arvore* inicializaArvore(){
	arvore* t = (arvore*)malloc(sizeof(arvore));
	t->raiz = NULL;
	return t;
}
no* alocarNo(int valor){
	no* novoNo = (no*)malloc(sizeof(no));
	novoNo->chave  = valor;
	novoNo->pai = NULL;
	novoNo->esq = NULL;
	novoNo->dir = NULL;
	return novoNo;
}
no* buscar(no* x, int k){
	if(x == NULL || k == x->chave){
		return x;
	}
	if(k < x->chave){
		return buscar(x->esq, k);
	} else {
		return buscar(x->dir, k);
	}
}

int maior (int x, int y){
	if(x > y){
		return x;
	} else {
		return y;
	}
}

int altura (no* x){
	if (x == NULL){
		return -1;	
	} else {
		return maior(altura(x->esq), altura(x->dir)) + 1;
	}
}

void inserir(arvore* t, no* z){
	no* y = NULL;
	no* x = t->raiz;
	while (x != NULL){
		y = x;
		if(z->chave < x->chave){
			x = y->esq;
		} else {
			x = x->dir;
		}
	}
	z->pai =y;
	if(y == NULL){
		t->raiz = z;
	} else if (z->chave < y->chave){
		y->esq = z;
	} else {
		y->dir = z;	
	}
	no* p = noDesbalanceado(z);
	if(p != NULL){
		balancear(t, p);
	}
}

no* noDesbalanceado(no* x){
	while (x != NULL){
		if(abs (altura(x->esq) - altura(x->dir)) > 1){
			return x;
		} else {
			x = x->pai;
		}
	}
	return NULL;
}

void rotacaoDireita (arvore* t, no* p){
	no* u = p->esq;
	if(u != NULL){
		p->esq = u->dir;
	}

	if(u->dir != NULL){
		u->dir->pai = p;
	}

	u->pai = p->pai;

	if(p->pai == NULL){
		t->raiz = u;	

	} else if (p == p->pai->esq){
			p->pai->esq = u;	
	} else {
		p->pai->dir = u;	
	}

	u->dir = p;
	p->pai = u;
}
void rotacaoEsquerda (arvore* t, no* p){
	no* u = p->dir;
	if(u != NULL){
		p->dir = u->esq;
	}

	if(u->esq != NULL){
		u->esq->pai = p;
	}

	u->pai = p->pai;

	if(p->pai == NULL){
		t->raiz = u;	

	} else if (p == p->pai->esq){
			p->pai->esq = u;	
	} else {
		p->pai->dir = u;	
	}

	u->esq = p;
	p->pai = u;
}
void rotacaoDuplaEsq (arvore* t, no* p){
	rotacaoDireita(t, p->dir);
	rotacaoEsquerda(t, p);
}
void rotacaoDuplaDir (arvore* t, no* p){
	rotacaoEsquerda(t, p->esq);
	rotacaoDireita(t, p);
}

void balancear (arvore* t, no* x){
	if(altura(x->esq) > altura(x->dir)){
		if(altura(x->esq->esq) > altura(x->esq->dir)){
			rotacaoDireita(t, x);
		} 
		else if(altura(x->esq->dir) > altura(x->esq->esq)){
			rotacaoDuplaDir(t, x);
		}
	}
	else if(altura(x->dir) > altura(x->esq)){
		if(altura(x->dir->dir) > altura(x->dir->esq)){
			rotacaoEsquerda(t, x);
		}
		else if(altura(x->dir->esq) > altura(x->dir->dir)){
			rotacaoDuplaEsq(t, x);
		}
	}
}

void percursoEmOrdem(no* x){
	if(x != NULL){
		percursoEmOrdem(x->esq);
		printf("%d ",x->chave);
		percursoEmOrdem(x->dir);
	}
}

void percursoPreOrdem(no* x){
	if(x != NULL){
		printf("%d ",x->chave);
		percursoPreOrdem(x->esq);
		percursoPreOrdem(x->dir);
	}
}

void percursoPosOrdem(no* x){
	if(x != NULL){
		percursoPosOrdem(x->esq);
		percursoPosOrdem(x->dir);
		printf("%d ",x->chave);
	}
}

no* maximo(no* x){
	if(x != NULL){
		while(x->dir != NULL){
			x = x->dir;
		}
	}
	return x;
}

no* minimo(no* x){
	if(x != NULL){
		while(x->esq != NULL){
			x = x->esq;
		}
	}
	return x;
}

no* sucessor(no* x){
	if(x != NULL){
		if(x->dir != NULL){
			return minimo(x->dir);
		}
		no* y = x->pai;
		while(y != NULL && x==y->dir){
			x = y;
			y = y->pai;
		}
		return y;
 
	}
}

no* antecessor(no* x){
	if(x != NULL){
		if(x->esq != NULL){
			return maximo(x->esq);
		}
		no* y = x->pai;
		while(y != NULL && x==y->esq){
			x = y;
			y = y->pai;
		}
		return y;
	}
}


