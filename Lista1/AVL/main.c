#include "AVL.h"

void main(){
	arvore* t = inicializaArvore();
	no* no1 = alocarNo(7);
	no* no2 = alocarNo(39);
	no* no3 = alocarNo(40);
	no* no4 = alocarNo(2);
	no* no5 = alocarNo(33);
	no* no6 = alocarNo(24);
	no* no7 = alocarNo(15);
	no* no8 = alocarNo(82);
	no* no9 = alocarNo(666);

	inserir(t, no1);
	inserir(t, no2);
	inserir(t, no3);
	inserir(t, no4);
	inserir(t, no5);
	inserir(t, no6);
	inserir(t, no7);
	inserir(t, no8);
	inserir(t, no9);

	printf("PERCURSO EM ORDEM: ");
	percursoEmOrdem(t->raiz);

	printf("\nPERCURSO PRE ORDEM: ");
	percursoPreOrdem(t->raiz);

	printf("\nPERCURSO POS ORDEM: ");
	percursoPosOrdem(t->raiz);
	
	printf("\nALTURA: ");
	int h = altura(t->raiz);
	printf("%d", h);
	
}
