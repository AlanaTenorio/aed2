#ifndef __AVL_H__
#define __AVL_H__

#include <stdio.h>
#include <stdlib.h>

struct cel {
	int chave;
	struct cel* pai;
	struct cel* esq;
	struct cel* dir;
}; typedef struct cel no;


struct arvore {
	no* raiz;
}; typedef struct arvore arvore;

arvore* inicializaArvore();
no* alocarNo(int valor);
no* buscar(no* x, int k);
int maior (int x, int y);
int altura (no* x);
void balancear (arvore* t, no* x);
void inserir(arvore* t, no* z);
void rotacaoDireita (arvore* t, no* p);
void rotacaoEsquerda (arvore* t, no* p);
void rotacaoDuplaEsq (arvore* t, no* p);
void rotacaoDuplaDir (arvore* t, no* p);
no* noDesbalanceado(no* no);
void percursoEmOrdem(no* x);
void percursoPreOrdem(no* x);
void percursoPosOrdem(no* x);
no* maximo(no* x);
no* minimo(no* x);
no* sucessor(no* x);
no* antecessor(no* x);

#endif // __AVL_C__
