#ifndef __HASH_H__
#define __HASH_H__

#include <stdio.h>
#include <stdlib.h>
#include "Lista.c"

#define m 12
struct hash{
	Lista* vetor[m];
}; typedef struct hash hash;

void hash_insert(hash* ha, int x);
void hash_delete(hash* ha, Lista* T, int x);
Lista* hash_buscar(hash* ha, Lista* T, int x);
int h(hash* ha, int k);

#endif // __HASH_H