struct lista {
       int info;
       struct lista* prox;
       struct lista* ant;
}; typedef struct lista Lista;

Lista* inicializar(){
       return NULL;
}

Lista* inserirComeco (Lista* li, int n){
        Lista* elemento = (Lista*) malloc (sizeof(Lista));
        elemento->info = n;
        elemento->prox = li;
        elemento->ant = NULL;
        if (li != NULL)
            li->ant = elemento;
        return elemento;
}

Lista* buscar (Lista* li, int n){
    Lista* i;
    for(i = li; i != NULL; i = i->prox){
        if(i->info == n){
            return i;
        }
    }
    return NULL;
}

void deletar (Lista* li, int n){
    Lista* i = buscar(li, n);
    if(i == NULL){
        fprintf(stderr, "Erro: elemento não encontrado");;
    } else {
        if (li == i){
            li = i->prox;
        } else {
            i->ant->prox = i->prox;
        }
        if(i->prox != NULL){
            i->prox->ant = i->ant;
        }
        free(i);
    }
}

void imprimirTodos(Lista* li){
     Lista* i;
     for(i = li; i != NULL; i = i->prox){
           printf("%d", i->info);
     }
}

