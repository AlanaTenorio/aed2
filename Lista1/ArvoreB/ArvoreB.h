#ifndef __ARVOREB_H__
#define __ARVOREB_H__

#include <stdio.h>
#include <stdlib.h>

#define t 2
#define MAX_CHAVES 2*t-1 //Número máximo de chaves
#define MAX_FILHOS 2*t //Número máximo de filhos
struct no {
	int num_chaves;
	int chaves[MAX_CHAVES];
	struct no* filhos[MAX_FILHOS];
	int folha; //1 para o caso de ser folha e 0 se não for
}; typedef struct no no;

struct arvore {
	no* raiz;
}; typedef struct arvore arvore;

struct tupla {
	no* n;
	int pos;
}; typedef struct tupla tupla;

no* alocarNo();
void criar(arvore* tree);
void split(no* x, int i);
void inserir_nonfull(no* x, int chave);
void inserir(arvore* tree, int chave);
tupla* buscar(no* x, int chave);
void printar(no* raiz);

#endif //__ARVOREB_H__
