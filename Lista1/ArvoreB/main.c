#include "ArvoreB.h"

void main(){
	arvore* tree = (arvore*)malloc(sizeof(arvore));
	//arvore* tree = NULL;
	criar(tree);
	inserir(tree, 300);
	inserir(tree, 20);
	inserir(tree, 150);
	inserir(tree, 430);
	inserir(tree, 480);
	inserir(tree, 520);
	inserir(tree, 12);
	inserir(tree, 25);
	inserir(tree, 80);
	inserir(tree, 142);
	inserir(tree, 176);
	inserir(tree, 206);
	inserir(tree, 297);
	inserir(tree, 380);
	inserir(tree, 395);
	inserir(tree, 412);
	inserir(tree, 451);
	inserir(tree, 493);
	inserir(tree, 506);
	inserir(tree, 521);
	inserir(tree, 600);

	printar(tree->raiz);

	tupla* tp = buscar(tree->raiz, 521);
	no* node = tp->n;
	int i = tp->pos;

	printf("Encontrou - %d\n", node->chaves[i]);
	
}
