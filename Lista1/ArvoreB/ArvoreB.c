#include "ArvoreB.h"

no* alocarNo(){
	no* novoNo = (no*)malloc(sizeof(no));
	return novoNo;
}

tupla* alocarTupla(){
	tupla* novaTupla = (tupla*)malloc(sizeof(tupla));
	return novaTupla;
}

void criar(arvore* tree){
	no* x = alocarNo();
	x->folha = 1;
	x->num_chaves = 0;
	tree->raiz = x;
}

void split(no* x, int i){
	int j;
	no* z = alocarNo();
	no* y = x->filhos[i];
	z->folha = y->folha;

	z->num_chaves = t-1;

	for (j = 0; j <= t-2; j++){
		z->chaves[j] = y->chaves[j+t];
	}

	if (y->folha == 0){
		for(j = 0; j <= t-1; j++){
			z->filhos[j] = y->filhos[j+t];
		}
	}

	y->num_chaves = t-1;

	for(j = x->num_chaves; j >= i+1; j--){
		x->filhos[j+1] = x->filhos[j];
	}

	x->filhos[i+1] = z;

	for(j = x->num_chaves-1; j >= i; j--){
		x->chaves[j+1] = x->chaves[j];
	}
	x->chaves[j+1] = y->chaves[t-1];
	x->num_chaves = (x->num_chaves)+1;
}

void inserir_nonfull(no* x, int chave){
	int i = x->num_chaves - 1;
	if (x->folha == 1){
		while (i >= 0 && chave < x->chaves[i]){
			x->chaves[i+1] = x->chaves[i];
			i = i -1;
		}
		x->chaves[i+1] = chave;
		x->num_chaves = x->num_chaves +1;
	} else {
		while(i >= 0 && chave < x->chaves[i]){
			i = i -1;
		}
		i = i + 1;
		if( x->filhos[i]->num_chaves == 2*t -1) {
			split(x, i);
			if (chave > x->chaves[i]){
				i = i + 1;
			}
		}
		inserir_nonfull(x->filhos[i], chave);
	}
}

void inserir(arvore* tree, int chave){
	no* x = tree->raiz;
	if (x->num_chaves == 2 * t -1){
		no* y = alocarNo();
		tree->raiz = y;
		y->folha = 0;
		y->num_chaves = 0;
		y->filhos[0] = x;
		split(y, 0);
		inserir_nonfull(y, chave);
	} else {
		inserir_nonfull(x, chave);
	}
}

tupla* buscar(no* x, int chave){
	int i = 0;
	while(i < x->num_chaves && chave > x->chaves[i]){
		i = i + 1;
	}
	if (i < x->num_chaves && chave == x->chaves[i]){
		tupla* tp = alocarTupla();
		tp->n = x;
		tp->pos = i;		
		return tp;
	} else if (x->folha == 1){
		return NULL;
	} else {
		return buscar(x->filhos[i], chave);
	}
}

void printar(no* raiz){
	int i = 0;
	if (raiz != NULL){
		for (i = 0; i < raiz->num_chaves; i++) {
			if(raiz->folha == 0){	
				printar (raiz->filhos[i]);
			}
			printf("%d\n", raiz->chaves[i]);
		}
		if(raiz->folha == 0){	
			printar (raiz->filhos[i]);
		}
	}
}
