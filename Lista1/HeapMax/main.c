#include "Heap.h"

void main(){
	heap* hp = (heap*) malloc (sizeof(heap));
	hp->tam = 0;

	max_heap_insert(hp, 8);
	max_heap_insert(hp, 5);
	max_heap_insert(hp, 90);
	max_heap_insert(hp, 14);
	max_heap_insert(hp, 2);
	max_heap_insert(hp, 97);
	max_heap_insert(hp, 500);
	max_heap_insert(hp, 1);
	max_heap_insert(hp, 20);
	max_heap_insert(hp, 39);
	
	printf("\nHeap ordenada:\n");
	mostrar(hp);
	int max = heap_maximum(hp);
	printf("\nValor máximo: ");
	printf("%d", max);
	int maxExtract = heap_extract_max(hp);
	printf("\nExtraindo: ");
	printf("%d", maxExtract);
	printf("\nHeap atual:\n");
	mostrar(hp);

	heap* hp2 = (heap*) malloc (sizeof(heap));
	hp2->tam = 0;
	insert(hp2, 6);
	insert(hp2, 23);
	insert(hp2, 110);
	insert(hp2, 7);
	insert(hp2, 4);
	insert(hp2, 14);
	insert(hp2, 1);
	insert(hp2, 70);
	insert(hp2, 65);
	insert(hp2, 30);
	build_max_heap(hp2);
	printf("\nHeap ordenada a partir do build_max:\n");
	mostrar(hp2);

	heap* hp3 = (heap*) malloc (sizeof(heap));
	hp3->tam = 0;
	insert(hp3, 8);
	insert(hp3, 20);
	insert(hp3, 11);
	insert(hp3, 1);
	insert(hp3, 3);
	insert(hp3, 4);
	insert(hp3, 115);
	insert(hp3, 70);
	insert(hp3, 32);
	insert(hp3, 95);
	heap_sort(hp3);
	printf("\nHeap ordenada com o heapSort:\n");
	mostrar(hp3);
}