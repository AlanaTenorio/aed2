#ifndef __HEAP_H__
#define __HEAP_H__

#include <stdio.h>
#include <stdlib.h>

#define comprimento 999999

struct  heap {
	int tam;
	int vetor[comprimento];
}; typedef struct heap heap;

int pai(int i);
int esq(int i);
int dir(int i);
void build_max_heap(heap* h);
void trocar(heap* h, int x, int y);
void max_heapify(heap* h, int i);
int heap_maximum(heap* h);
void heap_increase_key(heap* h, int i, int chave);
void max_heap_insert(heap* h, int chave);
void insert(heap* h, int chave);
void mostrar(heap* h);

#endif //__HEAP_H__
