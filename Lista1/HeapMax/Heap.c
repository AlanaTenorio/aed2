#include "Heap.h"

int pai(int i){
	return ((i+1)/2)-1;
}

int esq(int i){
	return (2*(i+1)-1);
}

int dir(int i){
	return 2*(i+1);
}

void build_max_heap(heap* h){
	int i;
	for (i = (comprimento)/2; i >= 0; i++){
		max_heapify(h, i);
	}
}

void trocar(heap* h, int x, int y){
	int aux; 
	aux = h->vetor[x];
	h->vetor[x] = h->vetor[y];
	h->vetor[y] = aux;
}

void max_heapify(heap* h, int i){
	int l = esq(i);
	int r = dir(i);
	int maior;
	if (l <= h->tam && h->vetor[l] > h->vetor[i]){
		maior = l;
	}
	else{
		maior = i;
	}
	if (r <= h->tam && h->vetor[r] > h->vetor[maior]){
		maior = r;
	}
	if (maior != i){
		trocar(h, i, maior);
		max_heapify(h, maior);
	}
}

int heap_maximum(heap* h){
	return h->vetor[0];
}

int heap_extract_max(heap* h){
	int max;
	if(h->tam < 0){
		fprintf(stderr, "\nErro");
	}
	h->tam = h->tam-1;
	max = h->vetor[0];
	h->vetor[0] = h->vetor[h->tam];
	max_heapify(h, 0);
	return max;
}

void heap_increase_key(heap* h, int i, int chave){
	if (chave < h->vetor[i]){
		fprintf(stderr, "\nErro");
	}
	h->vetor[i] = chave;
	while (i > 0 && h->vetor[pai(i)] < h->vetor[i]){
		trocar(h, i, pai(i));
		i = pai(i);
	}
}

void max_heap_insert(heap* h, int chave){
	h->tam = h->tam + 1;
	h->vetor[h->tam] = -9999999;
	heap_increase_key(h, h->tam-1, chave);
}

void heap_sort(heap* h){
	build_max_heap(h);
	int aux = h->tam;
	int i;
	for(i = (h->tam); i >= 1; i--){
		trocar(h, 0, i);
		h->tam = h->tam - 1;
		max_heapify(h, 0);
	}
	h->tam = aux;
}

void insert(heap* h, int chave){ //Insere de forma não ordenada
	h->vetor[h->tam] = chave;
	h->tam = h->tam + 1;
}

void mostrar(heap* h){
	int i;
	for (i = 0; i < h->tam; i++){
		printf("%d\n", h->vetor[i]);
	}
}

