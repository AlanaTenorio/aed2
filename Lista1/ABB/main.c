#include "ArvoreBin.h"

void main(){
	arvore* t = inicializaArvore();
	no* no1 = alocarNo(8);
	no* no2 = alocarNo(5);
	no* no3 = alocarNo(10);
	no* no4 = alocarNo(4);
	no* no5 = alocarNo(6);
	no* no6 = alocarNo(9);
	no* no7 = alocarNo(11);

	inserir(t, no1);
	inserir(t, no2);
	inserir(t, no3);
	inserir(t, no4);
	inserir(t, no5);
	inserir(t, no6);
	inserir(t, no7);
	
	printf("\nPERCURSO EM ORDEM: ");
	percursoEmOrdem(t->raiz);

	printf("\nPERCURSO PRE ORDEM: ");
	percursoPreOrdem(t->raiz);

	printf("\nPERCURSO PRE ORDEM: ");
	percursoPosOrdem(t->raiz);
	
	no* x = buscar(t->raiz, 10);
	printf("\nNO ENCONTRADO: ");
	printf ("%d", x->chave); 
	
	no* y = maximo(t->raiz);
	printf("\nMAXIMO: ");
	printf ("%d", y->chave);
	
	no* z = minimo(t->raiz);
	printf("\nMINIMO: ");
	printf ("%d", z->chave);

	no* w = sucessor(t->raiz);
	printf("\nSUCESSOR: ");
	printf ("%d", w->chave);

	no* a = antecessor(t->raiz);
	printf("\nANTECESSOR: ");
	printf ("%d", a->chave);
	
}
