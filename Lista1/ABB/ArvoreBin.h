#ifndef __ARVOREBIN_H__
#define __ARVOREBIN_H__

#include <stdio.h>
#include <stdlib.h>

struct cel {
	int chave;
	struct cel* pai;
	struct cel* esq;
	struct cel* dir;
}; typedef struct cel no;


struct arvore {
	no* raiz;
};
typedef struct arvore arvore;

arvore* inicializaArvore();
no* alocarNo(int valor);
no* buscar(no* x, int k);
void inserir(arvore* t, no* z);
void percursoEmOrdem(no* x);
void percursoPreOrdem(no* x);
no* maximo(no* x);
no* minimo(no* x);
no* sucessor(no* x);
no* antecessor(no* x);

#endif // __ARVOREBIN_C__
