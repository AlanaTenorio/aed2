#include "ArvoreBin.h"
#include <stdio.h>
#include <stdlib.h>

arvore* inicializaArvore(){
	arvore* t = (arvore*)malloc(sizeof(arvore));
	t->raiz = NULL;
	return t;
}
no* alocarNo(int valor){
	no* novoNo = (no*)malloc(sizeof(no));
	novoNo->chave  = valor;
	novoNo->pai = NULL;
	novoNo->esq = NULL;
	novoNo->dir = NULL;
	return novoNo;
}
no* buscar(no* x, int k){
	if(x == NULL || k == x->chave){
		return x;
	}
	if(k < x->chave){
		return buscar(x->esq, k);
	} else {
		return buscar(x->dir, k);
	}
}

void inserir(arvore* t, no* z){
	no* y = NULL;
	no* x = t->raiz;
	while (x != NULL){
		y = x;
		if(z->chave < x->chave){
			x = y->esq;
		} else {
			x = x->dir;
		}
	}
	z->pai =y;
	if(y == NULL){
		t->raiz = z;
	} else if (z->chave < y->chave){
		y->esq = z;
	} else {
		y->dir = z;	
	}
}

void percursoEmOrdem(no* x){
	if(x != NULL){
		percursoEmOrdem(x->esq);
		printf("%d ",x->chave);
		percursoEmOrdem(x->dir);
	}
}

void percursoPreOrdem(no* x){
	if(x != NULL){
		printf("%d ",x->chave);
		percursoPreOrdem(x->esq);
		percursoPreOrdem(x->dir);
	}
}

void percursoPosOrdem(no* x){
	if(x != NULL){
		percursoPosOrdem(x->esq);
		percursoPosOrdem(x->dir);
		printf("%d ",x->chave);
	}
}

no* maximo(no* x){
	if(x != NULL){
		while(x->dir != NULL){
			x = x->dir;
		}
	}
	return x;
}

no* minimo(no* x){
	if(x != NULL){
		while(x->esq != NULL){
			x = x->esq;
		}
	}
	return x;
}

no* sucessor(no* x){
	if(x != NULL){
		if(x->dir != NULL){
			return minimo(x->dir);
		}
		no* y = x->pai;
		while(y != NULL && x==y->dir){
			x = y;
			y = y->pai;
		}
		return y;
 
	}
}

no* antecessor(no* x){
	if(x != NULL){
		if(x->esq != NULL){
			return maximo(x->esq);
		}
		no* y = x->pai;
		while(y != NULL && x==y->esq){
			x = y;
			y = y->pai;
		}
		return y;
	}
}

