#include "RubroNegra.h"
#include <stdio.h>
#include <stdlib.h>

arvore* inicializaArvore(){
	arvore* t = (arvore*)malloc(sizeof(arvore));
	t->raiz = NULL;
	return t;
}
no* alocarNo(int valor){
	no* novoNo = (no*)malloc(sizeof(no));
	novoNo->chave  = valor;
	novoNo->red = 1;
	novoNo->pai = NULL;
	novoNo->esq = NULL;
	novoNo->dir = NULL;
	return novoNo;
}
no* buscar(no* x, int k){
	if(x == NULL || k == x->chave){
		return x;
	}
	if(k < x->chave){
		return buscar(x->esq, k);
	} else {
		return buscar(x->dir, k);
	}
}

int maior (int x, int y){
	if(x > y){
		return x;
	} else {
		return y;
	}
}

int alturaPreta(no* x){
	if(x == NULL){
		return -1;
	}
	int esq = alturaPreta(x->esq);
	int dir = alturaPreta(x->dir);
	int m = maior(esq, dir);
	if(x->red == 1){
		m++;
	}
	return m;
}

void inserir(arvore* t, no* z){
	no* y = NULL;
	no* x = t->raiz;
	while (x != NULL){
		y = x;
		if(z->chave < x->chave){
			x = y->esq;
		} else {
			x = x->dir;
		}
	}
	z->pai = y;
	if(y == NULL){
		t->raiz = z;
	} else if (z->chave < y->chave){
		y->esq = z;
	} else {
		y->dir = z;	
	}
	balancear(t, z);
}

void rotacaoDireita (arvore* t, no* p){
	no* u = p->esq;
	if(u != NULL){
		p->esq = u->dir;
	}

	if(u->dir != NULL){
		u->dir->pai = p;
	}

	u->pai = p->pai;

	if(p->pai == NULL){
		t->raiz = u;	

	} else if (p == p->pai->esq){
			p->pai->esq = u;	
	} else {
		p->pai->dir = u;	
	}

	u->dir = p;
	p->pai = u;
}
void rotacaoEsquerda (arvore* t, no* p){
	no* u = p->dir;
	if(u != NULL){
		p->dir = u->esq;
	}

	if(u->esq != NULL){
		u->esq->pai = p;
	}

	u->pai = p->pai;

	if(p->pai == NULL){
		t->raiz = u;	

	} else if (p == p->pai->esq){
			p->pai->esq = u;	
	} else {
		p->pai->dir = u;	
	}

	u->esq = p;
	p->pai = u;
}
void rotacaoDuplaEsq (arvore* t, no* p){
	rotacaoDireita(t, p->dir);
	rotacaoEsquerda(t, p);
}
void rotacaoDuplaDir (arvore* t, no* p){
	rotacaoEsquerda(t, p->esq);
	rotacaoDireita(t, p);
}
no* encontraTio(no* x){
	no* tio = NULL;
	if(x->pai != NULL){
		no* avo = x->pai->pai;
		if(avo != NULL && avo->esq != x->pai){
			tio = avo->esq;
		} else if (avo != NULL && avo->dir != x->pai){
			tio = avo->dir;
		}	
	}
	
	return tio;
}

void balancear (arvore* t, no* x){
	if(x->pai == NULL){
		x->red = 0;
		t->raiz = x;
	} else if (x->pai->red == 1){
		no* tio = encontraTio(x);
		if(tio != NULL && tio->red == 1){ //caso 1: o tio de x é vermelho
			x->pai->red = 0;
			tio->red = 0;
			x->pai->pai->red = 1;
			//x = x->pai->pai;
			balancear(t, x->pai->pai);
		} else if(x == x->pai->esq){ //caso 2: o tio de x é preto e x é filho a esquerda
			
			if (x->pai == x->pai->pai->esq){ //caso 2.1: o pai de x é filho a esquerda
				x->pai->red = 0;
				x->pai->pai->red = 1;
				rotacaoDireita(t, x->pai->pai);
				
			}
			else if (x->pai == x->pai->pai->dir){ //caso 2.2: o pai de x é filho a direita
				x->red = 0;
				x->pai->pai->red = 1;
				rotacaoDuplaEsq(t, x->pai->pai);
				
			}
		} else if (x == x->pai->dir) { //caso 3: o tio de x é preto e x é filho a direita
			if (x->pai == x->pai->pai->dir){ //caso 3.1: o pai de x é filho a direita
				x->pai->red = 0;
				x->pai->pai->red = 1;
				rotacaoEsquerda(t, x->pai->pai);
				
			}
			else if (x->pai == x->pai->pai->esq){ //caso 3.2: o pai de x é filho a esquerda
				x->red = 0;
				x->pai->pai->red = 1;
				rotacaoDuplaDir(t, x->pai->pai);
				
			}
		}
	}
}

void percursoEmOrdem(no* x){
	if(x != NULL){
		percursoEmOrdem(x->esq);
		printf("%d ",x->chave);
		percursoEmOrdem(x->dir);
	}
}

void percursoPreOrdem(no* x){
	if(x != NULL){
		printf("%d ",x->chave);
		percursoPreOrdem(x->esq);
		percursoPreOrdem(x->dir);
	}
}

void percursoPosOrdem(no* x){
	if(x != NULL){
		percursoPosOrdem(x->esq);
		percursoPosOrdem(x->dir);
		printf("%d ",x->chave);
	}
}

no* maximo(no* x){
	if(x != NULL){
		while(x->dir != NULL){
			x = x->dir;
		}
	}
	return x;
}

no* minimo(no* x){
	if(x != NULL){
		while(x->esq != NULL){
			x = x->esq;
		}
	}
	return x;
}

no* sucessor(no* x){
	if(x != NULL){
		if(x->dir != NULL){
			return minimo(x->dir);
		}
		no* y = x->pai;
		while(y != NULL && x==y->dir){
			x = y;
			y = y->pai;
		}
		return y;
 
	}
}

no* antecessor(no* x){
	if(x != NULL){
		if(x->esq != NULL){
			return maximo(x->esq);
		}
		no* y = x->pai;
		while(y != NULL && x==y->esq){
			x = y;
			y = y->pai;
		}
		return y;
	}
}

void testar (arvore* t, int chave){
	no* teste = buscar(t->raiz, chave);
	printf("\nNO SENDO TESTADO: ");
	printf("%d\n", teste->chave);
	printf("\nCOR: ");
	printf("%d\n", teste->red);
	if(teste->pai != NULL){
		printf("\nPAI: ");
		printf("%d\n", teste->pai->chave);
		printf("\nCOR DO PAI: ");
		printf("%d\n", teste->pai->red);
	} else {
		printf("\nNAO POSSUI PAI");
	}
	if(teste->esq != NULL){
		printf("\nFILHO A ESQUERDA: ");
		printf("%d\n", teste->esq->chave);	
	} else {
		printf("\nNAO POSSUI FILHO A ESQUERDA");
	}
	if (teste->dir != NULL){
		printf("\nFILHO A DIREITA: ");
		printf("%d\n", teste->dir->chave);
	} else {
		printf("\nNAO POSSUI FILHO A DIREITA");
	}
	no* tio = encontraTio(teste);
	if (tio != NULL){
		printf("\nTIO: ");
		printf("%d\n", tio->chave);
		printf("\nCOR DO TIO: ");
		printf("%d\n", tio->red);
	} else {
		printf("\nNAO POSSUI TIO");
	}

}
