#ifndef __RUBRONEGRA_H__
#define __RUBRONEGRA_H__

#include <stdio.h>
#include <stdlib.h>

struct cel {
	int chave;
	int red; //se o no for vermelho recebe 1 se o no for preto recebe 0
	struct cel* pai;
	struct cel* esq;
	struct cel* dir;
}; typedef struct cel no;


struct arvore {
	no* raiz;
}; typedef struct arvore arvore;

arvore* inicializaArvore();
no* alocarNo(int valor);
no* buscar(no* x, int k);
int maior (int x, int y);
int alturaPreta (no* x);
void balancear (arvore* t, no* x);
void inserir(arvore* t, no* z);
no* encontraTio(no* x);
void rotacaoDireita (arvore* t, no* p);
void rotacaoEsquerda (arvore* t, no* p);
void rotacaoDuplaEsq (arvore* t, no* p);
void rotacaoDuplaDir (arvore* t, no* p);
void percursoEmOrdem(no* x);
void percursoPreOrdem(no* x);
void percursoPosOrdem(no* x);
no* maximo(no* x);
no* minimo(no* x);
no* sucessor(no* x);
no* antecessor(no* x);
void testar (arvore* t, int chave);

#endif // __RUBRONEGRA_C__
