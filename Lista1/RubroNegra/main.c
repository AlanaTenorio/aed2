#include "RubroNegra.h"

void main(){
	arvore* t = inicializaArvore();
	/*no* no1 = alocarNo(11);
	no* no2 = alocarNo(2);
	no* no3 = alocarNo(14);
	no* no4 = alocarNo(1);
	no* no5 = alocarNo(7);
	no* no6 = alocarNo(15);
	no* no7 = alocarNo(5);
	no* no8 = alocarNo(8);
	no* no9 = alocarNo(4);*/

	no* no1 = alocarNo(11);
	no* no2 = alocarNo(14);
	no* no3 = alocarNo(15);
	no* no4 = alocarNo(2);
	no* no5 = alocarNo(1);
	no* no6 = alocarNo(7);
	no* no7 = alocarNo(5);
	no* no8 = alocarNo(8);
	no* no9 = alocarNo(4);

	inserir(t, no1);
	
	inserir(t, no2);
	
	inserir(t, no3);

	inserir(t, no4);
	
	inserir(t, no5);
	
	inserir(t, no6);
	
	inserir(t, no7);
	
	inserir(t, no8);
	
	inserir(t, no9);

	testar(t, 11);
	testar(t, 2);
	testar(t, 14);
	testar(t, 1);
	testar(t, 7);
	testar(t, 15);
	testar(t, 5);
	testar(t, 8);
	testar(t, 4);

	printf("PERCURSO EM ORDEM: ");
	percursoEmOrdem(t->raiz);

	printf("\nPERCURSO PRE ORDEM: ");
	percursoPreOrdem(t->raiz);

	printf("\nPERCURSO POS ORDEM: ");
	percursoPosOrdem(t->raiz);
	
}
